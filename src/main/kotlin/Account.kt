
const val ADDRESS_LENGTH_GLOB = 33

class Account {

    companion object {
        const val ADDRESS_LENGTH = 32
    }

    var address: String? = null
        get() //= field
        {
            if (field == null) {
                field = String(this.bytes!!, Charsets.UTF_8)
            }
            return field
        }

    var bytes: ByteArray? = null

    constructor(address: String) {
        this.address = address
        bytes = address.toByteArray(Charsets.UTF_8)
    }

    constructor(bytes: ByteArray) {
        this.bytes = bytes
    }

    // denied for external
    @Suppress("UNUSED_PARAMETER")
    private constructor(address: String, bytes: ByteArray) {
    }

}