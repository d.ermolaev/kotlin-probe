import com.natpryce.konfig.*
import org.json.simple.JSONArray
import org.json.simple.JSONValue
import java.io.File
import java.util.*

object Settings {

    val server_port = Key("server.port", intType)

    // --name=cli
    val cmdName = CommandLineOption(Key("name", stringType), "name", "name", "Description")
    val optY = Key("opt.y", stringType)
    val opts = arrayOf(
        cmdName,
        CommandLineOption(optY)
    )

    var vals = ConfigurationProperties.systemProperties() overriding
            EnvironmentVariables()

    var peers: Configuration? = null
    var knownPeers = JSONArray()
    var bannedPeers = JSONArray()

    fun update(args: Array<String>)  {

        val file = File("app.conf")
        println(file.absolutePath)

        if (file.exists())
            vals = ConfigurationProperties.fromFile(File("app.conf")) overriding
                    vals

        val res = parseArgs(args, *opts)

        vals = res.first overriding
                vals

        //TRY READ peers.conf
        peers = ConfigurationProperties.fromFile(File(getPeersPath()))
        val peersKnownKey = Key("known", stringType)
        //CREATE JSON OBJECT
        knownPeers = JSONValue.parse(peers?.get(peersKnownKey)) as JSONArray

        val peersBannedKey = Key("banned", stringType)
        if (peers?.contains(peersBannedKey) == true)
            bannedPeers = JSONValue.parse(peers?.get(peersBannedKey)) as JSONArray


    }

    @Throws(Exception::class)
    fun start() {}

    val NET_MODE_MAIN = 0
    val NET_MODE_CLONE = 1
    val NET_MODE_DEMO = 2
    val NET_MODE_TEST = 3
    var NET_MODE = 0

    private const val DEFAULT_MAINNET_STAMP = 1667260800000L // MAIN Net

    private const val DEFAULT_DEMO_NET_STAMP = 1667260800000L // DEMO Net

    private var userPath = ""
    val CLONE_OR_SIDE = "Chain" // cloneChain or SideChain

    fun isCloneNet(): Boolean {
        return NET_MODE == NET_MODE_CLONE
    }

    fun isDemoNet(): Boolean {
        return NET_MODE == NET_MODE_DEMO
    }

    fun isTestNet(): Boolean {
        return NET_MODE >= NET_MODE_DEMO
    }

    fun getPeersPath(): String {
        return userPath + if (isDemoNet()) "peers-demo.conf" else if (isTestNet()) "peers-test.conf" else if (isCloneNet()) CLONE_OR_SIDE.lowercase(
            Locale.getDefault()
        ) + "PEERS.conf" else "peers.conf"
    }

    operator fun <T> get(keyOpt: Any): T? {
        if (keyOpt is Key<*> && vals.contains(keyOpt)) {
            return vals[keyOpt] as T
        } else if (keyOpt is String && vals.contains(Key(keyOpt, stringType))) {
            return vals[Key(keyOpt, stringType)] as T
        }

        return null
    }

    operator fun contains(keyOpt: Any): Boolean {
        if (keyOpt is Key<*>) return vals.contains(keyOpt)
        if (keyOpt is String) return vals.contains(Key(keyOpt, stringType))
        return false
    }

}