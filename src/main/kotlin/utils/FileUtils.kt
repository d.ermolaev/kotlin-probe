package utils

import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.JSONValue
import java.io.File
import java.io.IOException
import java.nio.file.Files

object FileUtils {

    @Throws(IOException::class)
    fun readCommentedText(path: String?): String? {
        val file = File(path)
        if (!file.exists()) return null
        val lines: List<String>
        lines = try {
            Files.readAllLines(file.toPath(), Charsets.UTF_8)
        } catch (e: IOException) {
            throw IOException("Could not completely read file " + file.name)
        }
        var jsonString: String? = ""
        for (line in lines) {
            if (line.trim { it <= ' ' }.startsWith("//")) {
                continue
            }
            jsonString += line
        }
        return jsonString
    }

    @Throws(IOException::class)
    fun readCommentedJSONObject(path: String?): JSONObject? {
        val text: String = readCommentedText(path) ?: return JSONObject()

        //CREATE JSON OBJECT
        return JSONValue.parse(text) as JSONObject
    }

    @Throws(IOException::class)
    fun readCommentedJSONArray(path: String?): JSONArray? {
        val text: String = readCommentedText(path) ?: return JSONArray()

        //CREATE JSON ARRAY
        return JSONValue.parse(text) as JSONArray
    }

}