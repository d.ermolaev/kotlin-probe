import Settings.server_port
import com.natpryce.konfig.Key
import com.natpryce.konfig.stringType

fun main(args: Array<String>) {
    println("Hello World!")

    // Try adding program arguments via Run/Debug configuration.
    // Learn more about running applications: https://www.jetbrains.com/help/idea/running-applications.html.
    println("Program arguments: ${args.joinToString()}")

    Settings.update(args)

    println("server_port: ${Settings.vals[server_port]}")

    val name = Key("name", stringType)

    if (name in Settings) {
        println("name: ${Settings.vals[name]}")
        println(Settings["name"])
    }

    var port: Int? = Settings[server_port]
    if (port != null) port += 1
    println(port)

    println(Settings.knownPeers)
    println(Settings.bannedPeers)

    Settings.start()


}
